#ifndef helper_H_
#define helper_H_
#include"SDL2-2.0.3\include\SDL.h"
#include<string>
#include<array>
#include<utility>
using std::string;
using std::array;
namespace SdlHelper
{
	//SdlException
	//-toate exceptiile din namespace-ul SdlHelper
	// arunca aceasta exceptie in caz de eroare
	class SdlException
	{
		private:
			string message;
		public:
			SdlException(string argMessage) :message(argMessage){}
			string what(){ return message;}
	};
	//SdlWindow
	//-reprezinta o fereastra de pe ecran
	//-orice problema care apare este semnalata
	// prin aruncarea unei exceptii de tipul SdlException
	//!obiectul este mare => transmite prin referinta
	class SdlWindow
	{
		private:
			SDL_Window* window;
			SDL_Renderer* window_renderer;
			string title;
			int width;
			int height;
			int x;
			int y;
			int flag;
		public:
			//flag specifica diferite proprietati pentru fereastra
			//implicit flag = SLD_WINDOW_SHOWN
			SdlWindow(string argTitle, int argW, int argH, int argX, int argY, int argFlag = SDL_WINDOW_SHOWN);
			//constructor de copiere
			SdlWindow(const SdlWindow& source);
			//operator =&
			SdlWindow& operator=(const SdlWindow& source);
			//constructor de mutare
			SdlWindow(SdlWindow&& source);
			//operator =&&
			SdlWindow& operator=(SdlWindow&& source);
			//destructor
			~SdlWindow();
			//intoarce pointerul la fereastra
			//este folosit de alte obiecte pt desenare
			//!nu sterge pointerul returnat
			SDL_Renderer* getWindowRenderer()const{ return window_renderer; }
			//afiseaza in fereastra obiectele desenate
			void update(){ SDL_RenderPresent(window_renderer); }
			void clear(){ SDL_RenderClear(window_renderer); }

	};
	//SdlImage
	//-imagine in format bitmap
	//-pt alte tipuri de img foloseste un convertor
	class SdlImage
	{
		private:
			SDL_Texture* image_texture;
			SDL_Renderer* image_renderer;
			string path;
			SDL_Rect image_clip;
			SDL_Rect screen_clip;
			//incarca textura folosind
			//suprafata imaginii
			void loadTexture(SDL_Surface* image_surface);
		public:
			//arunca SdlException daca path nu e bun
			//image_clip este zona de decupare din imagine
			//screen_clip este zona de decupare din ecran
			//{} inseamna structura implicita, adica {0,0,0,0}
			SdlImage(string argPath, SDL_Renderer* windowRenderer, SDL_Rect argImageClip = {}, SDL_Rect argScreenClip = {});
			//primeste in plus r,g,b
			//pentru a defini culoarea de fundal(transparenta)
			SdlImage(string argPath, SDL_Renderer* windowRenderer, char red, char green, char blue, SDL_Rect argImageClip = {}, SDL_Rect argScreenClip = {});
			//constructori si operatori =
			SdlImage(const SdlImage& source);
			SdlImage& operator=(const SdlImage& source);
			SdlImage(SdlImage&& source);
			SdlImage& operator=(SdlImage&& source);
			//destructor
			~SdlImage();
			//-seteaza fereastra de vizualizare
			//-util atunci cand avem mai multe sprite-uri
			//pe aceeasi imagine
			void setImageClip(int x, int y, int w, int h)
			{
				image_clip.x = x; image_clip.y = y;
				image_clip.w = w; image_clip.h = h;
			}
			void setImageClip(SDL_Rect rect)
			{
				image_clip = rect;
			}
			//-muta imaginea pe ecran
			//!dupa aceea se apeleaza
			//image.load() si window.update()
			void move(int x, int y)
			{
				screen_clip.x = x; screen_clip.y = y;
			}
			//-incarca imaginea pe ecran
			//-in functie de factor se poate scala imaginea
			//window.update()
			void load(double factor=0.5);
			SDL_Rect getScreenClip()
			{
				return screen_clip;
			}
	};
	//SdlKeyboardListener
	//-interfata care trebuie implementata
	//pentru a asculta ev de la tastatura
	//-evenimente ce trebuie ascultate:
	//UP,DOWN,RIGHT,LEFT,SPACE, ESC
	class SdlKeyboardListener
	{
		public:
			virtual void onKeyDown()=0;
			virtual void onKeyUp() = 0;
			virtual void onKeyLeft() = 0;
			virtual void onKeyRight() = 0;
			virtual void onKeySpace() = 0;
			virtual void onKeyEsc() = 0;
			virtual SdlKeyboardListener* clone() = 0;
	};
	//SdlEventHandler
	//-trateaza evenimentele de tastatura
	//-copierea este costisitoare, foloseste &
	class SdlEventHandler
	{
		private:
			SdlKeyboardListener* key_listener;
			SDL_Event key_event;
		public:
			//-pointerul la SdlKeyboarLister 
			//va fi sters de destructor
			//!nu il dealoca in main()
			SdlEventHandler(SdlKeyboardListener* argKeyListener);
			SdlEventHandler(const SdlEventHandler& source);
			SdlEventHandler& operator=(const SdlEventHandler& source);
			SdlEventHandler(SdlEventHandler&& source);
			SdlEventHandler& operator=(SdlEventHandler&& source);
			~SdlEventHandler();
			//-face pool pentru urmatorul eveniment
			//-intoarce SDL_QUIT daca evenimentul este 
			//X de le fereastra
			//-intoarce 0 daca nu a aparut nici un eveniment
			int getEvent();
			//-in functie de tipul evenimentului
			//apeleaza functia corespunzatoare din 
			//SdlKeyboardListener
			void processEvent();

	};
	//SdlSpriteArray
	//-contine un array de dreptunghiuri
	//folosite pentru extragerea sprite-urilor
	//din aceeasi imagine
	template<int NR>
	class SdlSpriteArray
	{
		private:
			array<SDL_Rect, NR> sprites;
			int current_sprite;
			int size;
		public:
			//folosim constructorii, operatorii =
			//si destructorul generati implicit de compilator

			void addSprite(SDL_Rect argRect)
			{
				if (size<NR)
					sprites[size++] = argRect;
				else
				{
					SdlException e("Array plin");
					throw e;
				}
			}
			SDL_Rect getNextSprite()
			{
				SDL_Rect output = sprites[current_sprite];
				current_sprite++;
				if (current_sprite == size)
					current_sprite = 0;
				return output;
			}
	};
	//-functie helper
	//-creeaza un SDL_Rect din x,y,w,h
	SDL_Rect createClip(int x = 0, int y = 0, int w = 0, int h = 0);

	//-functie helper
	//-verifica daca exista coliziune intre
	//2 imagini
	//-dreptunghiurile se obtin cu getScreenClip()
	bool checkColision(SDL_Rect img1, SDL_Rect img2);
	
}
#endif