#include"SDL2-2.0.3\include\SDL.h"
#include"helper.h"
#include<string>
#include<iostream>
using namespace SdlHelper;
using std::string;
using std::cout;
class MyKeyboardListener :public SdlKeyboardListener
{
	private:
		SdlWindow window;
		SdlImage image;
		SdlImage snowman;
		SdlImage background;
		int x, y;
		SdlSpriteArray<10> sprites;
	public:
		MyKeyboardListener() :window("Test", 1000, 650, 50, 50), 
			image("imagini/skeleton.bmp", window.getWindowRenderer(), 255, 255, 255, SdlHelper::createClip(0,0,35,35), SdlHelper::createClip(0,0,35,35)),
			snowman("imagini/SNOWMAN.bmp", window.getWindowRenderer(), 255,255,255,SdlHelper::createClip(0, 0, 700, 700), SdlHelper::createClip(300,0,700,700)),
			x(0), y(0), background("imagini/summer.bmp", window.getWindowRenderer())
		{
			initializeSprites();
			background.load();
			snowman.load(8);
			image.load();
			window.update();
		}
		void initializeSprites()
		{
			SDL_Rect r{ 0, 65, 33, 33 };
			for (int i = 0; i < 10; i++)
			{
				r.x = i * 33;
				sprites.addSprite(r);
			}	
		}
		void onKeyDown(){}
		void onKeyUp(){}
		void onKeyLeft(){}
		void onKeyRight()
		{ 
			x = x + 5; window.clear();
			background.load(); 
			image.setImageClip(sprites.getNextSprite());
			image.move(x, y); 
			image.load();
			snowman.load(8);
			window.update();
			if (SdlHelper::checkColision(image.getScreenClip(), snowman.getScreenClip()))
				std::cout << "Coliziune\n";
		}
		void onKeySpace(){ cout << "space\n"; }
		void onKeyEsc(){ cout << "esc\n"; }
		SdlKeyboardListener* clone(){ return new MyKeyboardListener(*this); }
};
int main(int argc, char* argv[])
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		cout << "eroare la crearea programului\n";
		exit(1);
	}
	SdlEventHandler event_handler(new MyKeyboardListener);
	bool quit = false;
	while (quit == false)
	{
		//citeste toate evenimentele din coada
		int ev = event_handler.getEvent();
		while (ev != 0)
		{
			if (ev == SDL_QUIT)
				quit = true;
			event_handler.processEvent();
			ev = event_handler.getEvent();
		}

	}
	return 0;

}