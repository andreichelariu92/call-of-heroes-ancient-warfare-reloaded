#include"helper.h"
using namespace SdlHelper;
//~~~~~~~~~~~~~~Clasa SdlWindow~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SdlWindow::SdlWindow(string argTitle, int argW, int argH, int argX, int argY, int argFlag) :
	title(argTitle), width(argW), height(argH), x(argX), y(argY), flag(argFlag)
{
	//creaza fereastra
	//daca window = NULL arunca exceptie
	window = SDL_CreateWindow(title.c_str(), x, y, width, height, flag);
	if (window == NULL)
	{
		SdlException e("Eroare la crearea ferestrei");
		throw e;
	}
	//creare renderer pentru fereastra
	window_renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (window_renderer == NULL)
	{
		//sterge fereastra
		SDL_DestroyWindow(window);
		//arunca exceptie
		SdlException e("Eroare la crearea ferestrei");
		throw e;
	}
	//setare culoare alba la randare
	SDL_SetRenderDrawColor(window_renderer, 0, 0, 0, 0);
	
}
SdlWindow::SdlWindow(const SdlWindow& source):
	title(source.title), width(source.width), height(source.height), x(source.x), y(source.y), flag(source.flag)
{
	//creaza fereastra
	//daca window = NULL arunca exceptie
	window = SDL_CreateWindow(title.c_str(), x, y, width, height, flag);
	if (window == NULL)
	{
		SdlException e("Eroare la crearea ferestrei");
		throw e;
	}
	//creare renderer pentru fereastra
	window_renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (window_renderer == NULL)
	{
		//sterge fereastra
		SDL_DestroyWindow(window);
		//arunca exceptie
		SdlException e("Eroare la crearea ferestrei");
		throw e;
	}
	//setare culoare alba la randare
	SDL_SetRenderDrawColor(window_renderer, 0, 0, 0, 0);
}
SdlWindow& SdlWindow::operator=(const SdlWindow& source)
{
	//sterge informatia continuta in obiect
	//i.e. in pointerul this
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(window_renderer);
	//copie informatia din sursa
	title = source.title;
	width = source.width; height = source.height;
	x = source.x; y = source.y;
	flag = source.flag;
	//aloca memorie pt fereastra
	//creaza fereastra
	//daca window = NULL arunca exceptie
	window = SDL_CreateWindow(title.c_str(), x, y, width, height, flag);
	if (window == NULL)
	{
		SdlException e("Eroare la crearea ferestrei");
		throw e;
	}
	//creare renderer pentru fereastra
	window_renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (window_renderer == NULL)
	{
		//sterge fereastra
		SDL_DestroyWindow(window);
		//arunca exceptie
		SdlException e("Eroare la crearea ferestrei");
		throw e;
	}
	//setare culoare alba la randare
	SDL_SetRenderDrawColor(window_renderer, 0, 0, 0, 0);
	return *this;
}
SdlWindow::SdlWindow(SdlWindow&& source):
	title(source.title), width(source.width), height(source.height), x(source.x), y(source.y), flag(source.flag)
{
	//"furam" pointerii de la sursa
	window = source.window;
	window_renderer = source.window_renderer;
	//pointerii de la sursa ii facem NULL
	source.window = NULL;
	source.window_renderer = NULL;
}
SdlWindow& SdlWindow::operator=(SdlWindow&& source)
{
	//sterge informatia continuta in obiect
	//i.e. in pointerul this
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(window_renderer);

	title = source.title;
	width = source.width; height = source.height;
	x = source.x; y = source.y;
	flag = source.flag;
	//la fel ca mai sus
	window = source.window;
	window_renderer = source.window_renderer;
	source.window = NULL;
	source.window_renderer = NULL;
	return *this;
}
SdlWindow::~SdlWindow()
{
	if (window != NULL)
		SDL_DestroyWindow(window);
	if (window_renderer != NULL)
		SDL_DestroyRenderer(window_renderer);
}
//~~~~~~~~~~~~Clasa SdlImage~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SdlImage::loadTexture(SDL_Surface* image_surface)
{
	//incarca textura imaginii folosind suprafata ei
	image_texture = SDL_CreateTextureFromSurface(image_renderer, image_surface);
	//sterge suprafata imaginii
	//avem nevoie doar de textura ei
	SDL_FreeSurface(image_surface);
	//in caz de eroare arunca exceptie
	if (image_texture == NULL)
	{
		SdlException e("Eroare la deschiderea fisierului " + path);
		throw e;
	}
}
SdlImage::SdlImage(string argPath, SDL_Renderer* windowRender, SDL_Rect argImageClip, SDL_Rect argScreenClip):
path(argPath), image_renderer(windowRender), image_clip(argImageClip), screen_clip(argScreenClip)
{
	//incarca suprafata imaginii
	SDL_Surface* image_surface = SDL_LoadBMP(path.c_str());
	//in caz de eroare arunca exceptie
	if (image_surface == NULL)
	{
		SdlException e("Eroare la deschiderea fisierului " + path);
		throw e;
	}
	
	loadTexture(image_surface);
}
SdlImage::SdlImage(string argPath, SDL_Renderer* windowRenderer, char red, char green, char blue, SDL_Rect argImageClip, SDL_Rect argScreenClip)
	:path(argPath), image_renderer(windowRenderer), image_clip(argImageClip), screen_clip(argScreenClip)
{
	//incarca suprafata imaginii
	SDL_Surface* image_surface = SDL_LoadBMP(path.c_str());
	//in caz de eroare arunca exceptie
	if (image_surface == NULL)
	{
		SdlException e("Eroare la deschiderea fisierului " + path);
		throw e;
	}
	//culoarea de fundal(definita prin r, g, b)
	//va deveni transparenta
	SDL_SetColorKey(image_surface, SDL_TRUE, SDL_MapRGB(image_surface->format, red, green, blue));
	loadTexture(image_surface);
}
SdlImage::SdlImage(const SdlImage& source):
	path(source.path), image_renderer(source.image_renderer), image_clip(source.image_clip), screen_clip(source.screen_clip)
{
	//incarca suprafata imaginii
	SDL_Surface* image_surface = SDL_LoadBMP(path.c_str());
	//in caz de eroare arunca exceptie
	if (image_surface == NULL)
	{
		SdlException e("Eroare la deschiderea fisierului " + path);
		throw e;
	}
	loadTexture(image_surface);
}
SdlImage& SdlImage::operator=(const SdlImage& source)
{
	SdlImage copie(source);
	std::swap(*this, copie);
	return *this;
}
SdlImage::SdlImage(SdlImage&& source):
	path(source.path), image_renderer(source.image_renderer), image_clip(source.image_clip)
{
	//anuleaza path din sursa
	//pt a fi mai usor de sters
	source.path = "";
	//"fura" textura din sursa
	image_texture = source.image_texture;
	source.image_texture = NULL;
}
SdlImage& SdlImage::operator=(SdlImage&& source)
{
	//sterge imaginea existenta
	if (image_texture)
		SDL_DestroyTexture(image_texture);
	//exact ca mai sus
	path = source.path;
	image_renderer = source.image_renderer;
	image_clip = source.image_clip;
	//anuleaza path din sursa
	//pt a fi mai usor de sters
	source.path = "";
	//"fura" suprafata din sursa
	image_texture = source.image_texture;
	source.image_texture = NULL;
	return *this;
}
SdlImage::~SdlImage()
{
	if (image_texture == NULL)
		SDL_DestroyTexture(image_texture);
	//renderul apratine ferestrei
	//va fi sters de destrucotrul clasei
	//SdlWindow
}
void SdlImage::load(double factor)
{
	//implicit afiseaza imaginea pe tot ecranul
	if (image_clip.x == 0 && image_clip.y == 0 && image_clip.w == 0 && image_clip.h == 0)
		SDL_RenderCopy(image_renderer, image_texture, NULL, NULL);
	else
	{
		SDL_Rect scaled_screen_clip = screen_clip;
		scaled_screen_clip.w = scaled_screen_clip.w / factor;
		scaled_screen_clip.h = scaled_screen_clip.h / factor;
		SDL_RenderCopy(image_renderer, image_texture, &image_clip, &scaled_screen_clip);
	}
}
//~~~~~~~~~~~~~~Clasa SdlEventHandler~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SdlEventHandler::SdlEventHandler(SdlKeyboardListener* argKeyListener)
{
	key_listener = argKeyListener;
}
SdlEventHandler::SdlEventHandler(const SdlEventHandler& source)
{
	key_listener = source.key_listener->clone();
}
SdlEventHandler& SdlEventHandler::operator=(const SdlEventHandler& source)
{
	key_listener = source.key_listener->clone();
	return *this;
}
SdlEventHandler::SdlEventHandler(SdlEventHandler&& source)
{
	key_listener = source.key_listener;
	source.key_listener = 0;
}
SdlEventHandler& SdlEventHandler::operator=(SdlEventHandler&& source)
{
	key_listener = source.key_listener;
	source.key_listener = 0;
	return *this;
}
SdlEventHandler::~SdlEventHandler()
{
	if (key_listener)
		delete key_listener;
}
int SdlEventHandler::getEvent()
{
	if (SDL_PollEvent(&key_event) == 0)
		return 0;
	else
		return key_event.type;
}
void SdlEventHandler::processEvent()
{
	if (key_event.type == SDL_KEYDOWN)
	{
		//apeleaza metoda corespunzatoare
		//a interfetei SdlKeyboardListener
		switch (key_event.key.keysym.sym)
		{
			case SDLK_UP:
				key_listener->onKeyUp();
				break;
			case SDLK_DOWN:
				key_listener->onKeyDown();
				break;
			case SDLK_LEFT:
				key_listener->onKeyLeft();
				break;
			case SDLK_RIGHT:
				key_listener->onKeyRight();
				break;
			case SDLK_SPACE:
				key_listener->onKeySpace();
				break;
			case SDLK_ESCAPE:
				key_listener->onKeyEsc();
				break;
		}
	}
}
//~~~~~~~~~~~~~~~~Functii helper~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SDL_Rect SdlHelper::createClip(int x, int y, int w, int h)
{
	SDL_Rect output{ x, y, w, h };
	return output;
}
bool SdlHelper::checkColision(SDL_Rect img1, SDL_Rect img2)
{
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	leftA = img1.x;
	rightA = img1.x + img1.w;
	topA = img1.y;
	bottomA = img1.y + img1.h;
	leftB = img2.x;
	rightB = img2.x + img2.w;
	topB = img2.y;
	bottomB = img2.y + img2.h;

	if (topA > bottomB)
		return false;
	if (bottomA < topB)
		return false;
	if (rightA < leftB)
		return false;
	if (leftA > rightB)
		return false;
	return true;

}